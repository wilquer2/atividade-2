#!/bin/bash

echo '$0' = "Retorna o nome do programa executado" 
echo '$1' = "A variavel sendo inserida na função"
echo '$2' = "O segundo parâmetro"
echo '$#' = "Retorna o número de argumentos que o pragrama recebeu"
echo '$*' = "Retorna todos os argumentos informados na execução do programa"

